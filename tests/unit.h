#ifndef TESTS_UNIT_H
#define TESTS_UNIT_H

#define try_stmt(i, stmt, msg) do {                          \
    try {                                                    \
        stmt;                                                \
    } catch (std::exception& e) {                            \
        std::cerr << e.what() << '\n';                       \
        std::cout << "not ";                                 \
    }                                                        \
    std::cout << "ok " << (i) << " - " << (msg) << '\n';     \
} while (0);

#define assert_eq(i, a, b, msg) do {                     \
    if ((a) != (b)) {                                    \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#define assert_ne(i, a, b, msg) do {                     \
    if ((a) == (b)) {                                    \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#define assert_lt(i, a, b, msg) do {                     \
    if ((a) >= (b)) {                                    \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#define assert_gt(i, a, b, msg) do {                     \
    if ((a) <= (b)) {                                    \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#define assert_ge(i, a, b, msg) do {                     \
    if ((a) < (b)) {                                     \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#define assert_le(i, a, b, msg) do {                     \
    if ((a) < (b)) {                                     \
        std::cout << "not ";                             \
    }                                                    \
    std::cout << "ok " << (i) << " - " << (msg) << '\n'; \
} while (0);

#endif // TESTS_UNIT_H
