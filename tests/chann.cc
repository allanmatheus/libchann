#include <array>
#include <algorithm>
#include <chrono>
#include <future>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <thread>
#include <vector>
#include "../src/chann.h"
#include "util.h"
#include "unit.h"

void test_version();
void test_chann_creation();
void test_push();
void test_empty_stringfy();
void test_push_pop();
void test_mt_push_pop();
void test_close();
void test_push_close_pop();
void test_close_push();
void test_flush();
void test_close_flush();
void test_flush_empty();
void test_close_flush();

static std::size_t test_num = 0;

int main(int argc, char **argv)
{
    std::cout << "1..17\n";
    test_version();
    test_chann_creation();
    test_push();
    test_empty_stringfy();
    test_push_pop();
    test_mt_push_pop();
    test_push_close_pop();
    test_close_push();
    test_flush_empty();
    test_close_flush();
    return 0;
}

void test_version()
{
    std::string version{chann_version()};
    assert_eq(++test_num, version, "0.0.1", "check for the current version");
}

void test_chann_creation()
{
    try_stmt(++test_num, chann<int> c, "Default ctor and dtor");
}

void test_push()
{
    chann<int> ch;
    int x = 3;
    ch.push(x);
    assert_eq(++test_num, ch.size(), 1, "push an element (T&)");
    ch.push(5);
    assert_eq(++test_num, ch.size(), 2, "push a literal element (T&&)");
}

void test_empty_stringfy()
{
    chann<int> ch;
    std::string expected{"{ }"};

    std::stringstream ss;
    ss << ch;
    std::string result{ss.str()};

    assert_eq(++test_num, result, expected, "stringfy an empty channel");
}

void test_push_pop()
{
    chann<int> ch;

    int x = 3;
    ch.push(x);
    ch.push(5);

    int y1, y2;

    assert_eq(++test_num, ch.pop(y1), true, "pop a valid element");
    assert_eq(++test_num, y1, x, "pop an element (T&)");

    assert_eq(++test_num, ch.pop(y2), true, "pop a valid element");
    assert_eq(++test_num, y2, 5, "pop a literal element (T&&)");
}

void mt_push_worker(chann<int>& ch, int b, int e)
{
    for (int i = b; i < e; ++i) {
        ch.push(i);
    }
}

void test_mt_push_pop()
{
    constexpr int nthds = 4;
    constexpr int njobs = 100;

    chann<int> ch;
    std::array<int, njobs> x;
    std::list<std::thread> thds;

    for (int i = 0; i < nthds; ++i) {
        auto b = i * (njobs / nthds);
        auto e = (i + 1) * (njobs / nthds);
        std::thread t{mt_push_worker, std::ref(ch), b, e};
        thds.push_back(std::move(t));
    }

    for (int i = 0; i < njobs && ch.pop(x[i]); ++i)
        ;

    for (auto t = begin(thds); t != end(thds); ++t) {
        t->join();
    }

    std::sort(begin(x), end(x));

    for (int i = 0; i < njobs; ++i) {
        if (x[i] != i) {
            std::cout << "not ";
            break;
        }
    }

    std::cout << "ok " << ++test_num << " - multi-thread constructed array\n";
}

void test_push_close_pop()
{
    chann<int> ch;

    int x = 3;
    ch.push(x);

    ch.close();

    int y1;
    assert_eq(++test_num, ch.pop(y1), true, "pop a valid element");
    assert_eq(++test_num, y1, x, "pop an element (T&)");

    assert_eq(++test_num, ch.pop(y1), false, "pop an invalid element");

    try {
        ch.pop();
        std::cout << "not ok " << ++test_num
            << " - pop from a closed empty channel\n";

    } catch (closed_channel& e) {
        std::cout << "ok " << ++test_num << " - " << e.what() << '\n';
    }
}

void test_close_push()
{
    chann<int> ch;

    ch.close();

    try {
        int x = 3;
        ch.push(x);

        std::cout << "not ok " << ++test_num
            << " - push to a closed channel\n";

    } catch (closed_channel& e) {
        std::cout << "ok " << ++test_num << " - " << e.what() << '\n';
    }
}

void test_copy_value()
{
    constexpr std::size_t n = 10;

    copyable cp{n};
    chann<copyable> ch;

    for (std::size_t i = 0; i < n; ++i) {
        cp.x[i] = 2 * i + 1;
    }

    ch.push(cp);

    copyable cp2;
    assert_eq(++test_num, ch.pop(cp2), true, "pop a valid element");
    assert_eq(++test_num, cp.sz, cp2.sz, "copies have the same size");
    assert_ne(++test_num, &cp.x[0], &cp2.x[0],
            "copies have different mem addr");
}

void test_copy_value_by_return()
{
    constexpr std::size_t n = 10;

    copyable cp{n};
    chann<copyable> ch;

    for (std::size_t i = 0; i < n; ++i) {
        cp.x[i] = 2 * i + 1;
    }

    ch.push(cp);

    try {
        copyable cp2 = ch.pop();
        std::cout << "ok " << ++test_num << " - pop a valid element\n";

        assert_eq(++test_num, cp.sz, cp2.sz, "copies have the same size");
        assert_ne(++test_num, &cp.x[0], &cp2.x[0],
                "copies have different mem addr");

    } catch (closed_channel& e) {
        std::cout << "not ok " << ++test_num << " - " << e.what() << '\n';
    }
}

void test_move_value()
{
    constexpr std::size_t n = 10;

    moveable mv{n};
    chann<moveable> ch;

    for (std::size_t i = 0; i < n; ++i) {
        mv.x[i] = 2 * i + 1;
    }

    ch.push(std::move(mv));

    moveable mv2;
    assert_eq(++test_num, ch.pop(mv2), true, "pop a valid element");
    assert_eq(++test_num, mv.sz, 0, "source has no value");
    assert_eq(++test_num, mv2.sz, n, "source has everything");
}

void test_move_value_by_return()
{
    constexpr std::size_t n = 10;

    moveable mv{n};
    chann<moveable> ch;

    for (std::size_t i = 0; i < n; ++i) {
        mv.x[i] = 2 * i + 1;
    }

    ch.push(std::move(mv));

    try {
        moveable mv2 = ch.pop();
        std::cout << "ok " << ++test_num << " - pop a valid element\n";

        assert_eq(++test_num, mv.sz, 0, "source has no value");
        assert_eq(++test_num, mv2.sz, n, "source has everything");

    } catch (closed_channel& e) {
        std::cout << "not ok " << ++test_num << " - " << e.what() << '\n';
    }
}

void test_flush_empty()
{
    chann<int> ch;
    auto flusher = std::async(std::launch::async, &chann<int>::flush, &ch);
    auto status = flusher.wait_for(std::chrono::milliseconds(10));
    assert_eq(++test_num, status, std::future_status::ready,
            "flush an empty channel should be immediate");
}

void test_close_flush()
{
    chann<int> ch;
    ch.push(5);
    ch.close();
    auto flusher = std::async(std::launch::async, &chann<int>::flush, &ch);
    ch.pop();
    auto status = flusher.wait_for(std::chrono::milliseconds(10));
    assert_eq(++test_num, status, std::future_status::ready,
            "flush a closed channel");
}
