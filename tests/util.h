#ifndef TESTS_UTIL_H
#define TESTS_UTIL_H

#include <ostream>

std::ostream& operator<<(std::ostream& o, const std::vector<double>& v)
{
    o << "{ ";
    for (auto i = begin(v); i != end(v); ++i) {
        o << *i << ' ';
    }
    o << '}';
    return o;
}

struct copyable {
    double *x;
    std::size_t sz;

    explicit copyable(std::size_t n = 0)
        : x(n ? new double[n] : nullptr), sz(n)
    { }

    ~copyable()
    {
        if (x) {
            delete[] x;
            sz = 0;
        }
    }

    copyable(const copyable& cp) : x(new double[cp.sz]), sz(cp.sz)
    {
        std::cerr << "cp copy ctor\n";
        for (std::size_t i = 0; i < sz; ++i) {
            x[i] = cp.x[i];
        }
    }

    copyable& operator=(const copyable& cp)
    {
        std::cerr << "cp copy op=\n";
        if (this != &cp) {
            delete[] x;
            sz = cp.sz;
            for (std::size_t i = 0; i < sz; ++i) {
                x[i] = cp.x[i];
            }
        }
        return *this;
    }

    bool operator!=(const copyable& cp) const
    {
        return !(*this == cp);
    }

    bool operator==(const copyable& cp) const
    {
        if (sz != cp.sz) {
            return false;
        }

        for (std::size_t i = 0; i < sz; ++i) {
            if (x[i] != cp.x[i]) {
                return false;
            }
        }

        return true;
    }
};

struct moveable {
    double *x;
    std::size_t sz;

    explicit moveable(std::size_t n = 0)
        : x(n ? new double[n] : nullptr), sz(n)
    { }

    ~moveable()
    {
        if (x) {
            delete[] x;
            sz = 0;
        }
    }

    moveable(const moveable& mv) = delete;
    moveable& operator=(const moveable& mv) = delete;

    moveable(moveable&& mv)
    {
        std::cerr << "mv move ctor\n";
        sz = mv.sz;
        x = mv.x;
        mv.sz = 0;
        mv.x = nullptr;
    }

    moveable& operator=(moveable&& mv)
    {
        std::cerr << "mv move op=\n";
        if (this != &mv) {
            sz = mv.sz;
            x = mv.x;
            mv.sz = 0;
            mv.x = nullptr;
        }
        return *this;
    }

    bool operator!=(const moveable& mv) const
    {
        return !(*this == mv);
    }

    bool operator==(const moveable& mv) const
    {
        if (sz != mv.sz) {
            return false;
        }

        for (std::size_t i = 0; i < sz; ++i) {
            if (x[i] != mv.x[i]) {
                return false;
            }
        }

        return true;
    }
};

#endif // TESTS_UTIL_H
